# Desafio Lumis

## Tecnologias utilizadas

Esse sistema foi desenvolvido usando as seguintes tecnologias:

- Java 8
- Tomcat 9
- Maven 3.5.2
- Git
- SQLite 3
- Eclipse (jee-2018-09)

O sistema foi desenvolvido em um sistema operacional linux (kubuntu 18.04)

## Instalação

Antes de instalar o sistema, deve-se instalar o java 8 e o Tomcat 9 no sistema operacional.

>**Instalação do Java 8**  
>No Linux: https://www.edivaldobrito.com.br/oracle-java-no-ubuntu-18-04-lts/
>
>No windows:
https://www.java.com/pt_BR/download/help/windows_manual_download.xml


>**Instalação do Tomcat 9**  
>No Linux: https://linuxize.com/post/how-to-install-tomcat-9-on-ubuntu-18-04/#step-2-create-tomcat-user
>
>No windows:
http://www.ntu.edu.sg/home/ehchua/programming/howto/tomcat_howto.html

### Usando o arquivo `desafio.war`

Copie o arquivo `desafio.war` para o na pasta `webapps` do tomcat, depois vá na pasta `bin` do tomcat e execute `startup.sh` no linux ou `startup.bat` no windows.

Abra um navegador, e digitar na barra de endereço:

`http://localhost:8080/desafio`

e a página inicial deve aparecer

### Usando o git

Antes é necessário instalar o git

>Linux: https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-18-04-quickstart  
>Windows: https://woliveiras.com.br/posts/instalando-o-git-windows/

Utilize o seguinte comando para criar e copiar a pasta do projeto

```
git clone https://carlos-alexandre@bitbucket.org/carlos-alexandre/desafio.git 
```

entre na pasta criada, `cd desafio`, e execute o comando:  
- Linux `./mvnw install` 
- Windows `mvnw.cmd`

> Se o comando acima não funcionar, deve-se instalar o maven e executar o comando `mvn` ao invés do `mvnw`

Após isso copie o arquivo `target/desafio.war` para o na pasta `webapps` do tomcat, depois vá na pasta `bin` do tomcat e execute `startup.sh` no linux ou `startup.bat` no windows.

Abra um navegador, e digitar na barra de endereço:

`http://localhost:8080/desafio`

e a página inicial deve aparecer

### Importando no eclipse

Abra o eclipse, no menu siga:

1. File
2. Import...
3. Maven
4. Existing Maven Project
5. botão Next>
6. botão Browser e selecione a  pasta do projeto desafio
7. Selecione o pom
8. Botão Finish

depois adicione o servidor tomcat seguindo os passos, no menu:

1. Window 
2. show view 
3. Servers
4. Na janela Servers click em `No server are avaliabe.(...)`
5. Clique em `Apache` 
6. Escolha `Apache tomcat v9.0`
7. botão `Next>`
8. botão `Browser` e escolha a pasta onde o tomcat foi instalado
9. botão `Next`
10. clique `desafio`
11. botão `Add`
12. botão `Finish`

#### Baixando as dependências do projeto

1. Clique com o botão direito em `desafio`
2. Escolha `Maven` 
3. `Update Project` 
4. clique em `desfio`
5. `OK`
>**IMPORTANTE**  
> Algumas vezes o eclipse após fazer o `Update Project` acaba desconfigurando o processo de empacotamento o que leva a um erro durante a execução do sistema, para  evitar isso, deve-se verificar o classpath, conforme abaixo.  
> **Verificando o classpath**  
> 1. Clique com o botão direito em `desafio`
> 2. No menu escolha `properties` 
> 3. Depois escolha `Deployment Assembly` 
> 4. Verificar na lista se tem a opção `Maven Dependecies`, **se existir** pule para o passo _9_ 
> 5. Clicar em `Add...`
> 6. Escolher `Java Build Path Entries`
> 7. botão `Next>`
> 8. Escolher `Maven Dependecies`
> 9. botão `Finish`
> 10. botão `Apply and Close`
>
> _Mais sobre esse problema em:_ https://stackoverflow.com/questions/20718566/maven-dependencies-not-being-added-to-web-inf-lib

#### Executando o sistema

1. Clique com o botão direito em `desafio`
2. Escolha `Run as`
3. depois `maven install`
4. ao terminar a execução, clique com o botão direito em `desafio` novamente
5. Escolha `Run as` 
6. Opção `Run on Server`
7. Selecione `Apache tomcat v9.0 at localhost`
8. Clique no botão `Finish`

Se o navegar não executar automaticamente, abra um navegador, e digitar na barra de endereço:

`http://localhost:8080/desafio`

e a página inicial deve aparecer

### Usuários e senhas

Para o login do sistema utilizar como conta de administrador:
> **login:** admin  
> **senha:** admin

E como conta de atendimento:
> **login:** atend  
> **senha:** atend

## Notas sobre o banco de dados

O banco de dados utilizado foi o SQLite, e está configurado para gerar um arquivo chamado `desafio.db` e fica localizado na pasta onde o tomcat é iniciado.

Se o tomcat for iniciado via eclipse, o arquivo será criado na pasta do usuário (/home/..., no linux), e
se o tomcat for iniciado usando os scripts de inicialização localizado na pasta `bin`, o banco será criando na pasta `bin`.
