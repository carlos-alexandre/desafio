package br.com.lumis.desafio.validator;

import org.apache.commons.validator.GenericValidator;

import br.com.lumis.desafio.validator.error.ValidatorError;

public class EmailValidator extends ValidatorDecorator {

	public EmailValidator(Validator validator) {
		super(validator);
	}

	@Override
	public boolean validate() {
		return (super.validate() && emailValidation());
	}

	private boolean emailValidation() {
		return GenericValidator.isEmail(this.getValor());
	}

	@Override
	public ValidatorError getError() {
		ValidatorError error = super.getError();
		if(error == null && !emailValidation()) {
			error = new ValidatorError(this.getNome(),
					String.format("O E-mail %s é inválido", this.getValor())
			);
		}
		return error;
	}
}
