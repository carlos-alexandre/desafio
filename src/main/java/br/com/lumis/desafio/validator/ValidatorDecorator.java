package br.com.lumis.desafio.validator;

import br.com.lumis.desafio.validator.error.ValidatorError;

public abstract class ValidatorDecorator implements Validator {

	private Validator validator;
	private ValidatorError error;

	public ValidatorDecorator(Validator validator) {
		this.validator = validator;
	}

	@Override
	public boolean validate() {
		return this.validator.validate();
	}

	@Override
	public String getNome() {
		return this.validator.getNome();
	}

	public String getValor() {
		return this.validator.getValor();
	}

	public ValidatorError getError() {
		return this.validator.getError();
	}
}
