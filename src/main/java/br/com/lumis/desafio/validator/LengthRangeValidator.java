package br.com.lumis.desafio.validator;

import org.apache.commons.validator.GenericValidator;

import br.com.lumis.desafio.validator.error.ValidatorError;

public class LengthRangeValidator extends ValidatorDecorator {
	private int minLen;
	private int maxLen;

	public LengthRangeValidator(int minLen, int maxLen, Validator validator) {
		super(validator);
		this.minLen = minLen;
		this.maxLen = maxLen;
	}

	@Override
	public boolean validate() {
		return (super.validate() && lengthRangeValidation());
	}

	private boolean lengthRangeValidation() {
		return (GenericValidator.maxLength(this.getValor(), this.maxLen)
				&& GenericValidator.minLength(this.getValor(), this.minLen));

	}

	@Override
	public ValidatorError getError() {
		ValidatorError error = super.getError();
		if(error == null && !lengthRangeValidation()) {
			error = new ValidatorError(this.getNome(), 
					String.format("O tamanho do campo %s deve estar entre %d e %d", this.getNome(), this.minLen, this.maxLen)
			);
		}
		return error;
	}
}
