package br.com.lumis.desafio.validator;

import br.com.lumis.desafio.validator.error.ValidatorError;
import lombok.Getter;

public class BaseValidator implements Validator {
	@Getter
	private String nome;
	@Getter
	private String valor;

	public BaseValidator(String nome, String valor) {
		if (nome != null)
			this.nome = nome.trim();
		if (valor != null)
			this.valor = valor.trim();
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public ValidatorError getError() {
		return null;
	}

}
