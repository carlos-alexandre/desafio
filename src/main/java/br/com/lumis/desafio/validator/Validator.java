package br.com.lumis.desafio.validator;

import br.com.lumis.desafio.validator.error.ValidatorError;

public interface Validator {
	public boolean validate();
	public String getNome();
	public String getValor();
	public ValidatorError getError();
}
