package br.com.lumis.desafio.validator;

import org.apache.commons.validator.GenericValidator;

import br.com.lumis.desafio.validator.error.ValidatorError;

public class TelefoneValidator extends ValidatorDecorator {

	public TelefoneValidator(Validator validator) {
		super(validator);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validate() {
		return (super.validate() && telefoneValidation());
	}

	private boolean telefoneValidation() {
		return GenericValidator.matchRegexp(this.getValor(), "\\(\\d{2}\\)\\s{0,1}[9]{0,1}\\d{4}-\\d{4}");
	}

	@Override
	public ValidatorError getError() {
		ValidatorError error = super.getError();
		if (error == null && !telefoneValidation()) {
			error = new ValidatorError(this.getNome(), String.format("O Telefone %s é inválido", this.getValor()));
		}
		return error;
	}
}
