package br.com.lumis.desafio.validator.error;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValidatorError {
	private String element;
	private String mensage;
}
