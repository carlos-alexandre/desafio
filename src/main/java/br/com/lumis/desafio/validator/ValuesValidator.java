package br.com.lumis.desafio.validator;

import java.util.Arrays;

import br.com.lumis.desafio.validator.error.ValidatorError;

public class ValuesValidator extends ValidatorDecorator {
	private String[] values;

	public ValuesValidator(String values, Validator validator) {
		super(validator);
		this.values = values.split(",");
	}

	@Override
	public boolean validate() {
		return (super.validate() && valuesValidation());
	}

	private boolean valuesValidation() {
		return Arrays.stream(this.values).anyMatch(this.getValor()::equals);
	}

	@Override
	public ValidatorError getError() {
		ValidatorError error = super.getError();
		if (error == null && valuesValidation()) {
			error = new ValidatorError(this.getNome(),
					String.format("O campo %s não é dos valores %s", this.getNome(), String.join(", ", this.values)));
		}
		return error;
	}
}
