package br.com.lumis.desafio.validator;

import org.apache.commons.validator.GenericValidator;

import br.com.lumis.desafio.validator.error.ValidatorError;

public class RequiredValidator extends ValidatorDecorator {

	public RequiredValidator(Validator validator) {
		super(validator);
	}

	@Override
	public boolean validate() {
		return (super.validate() && requiredValidation());
	}

	private boolean requiredValidation() {
		return (!GenericValidator.isBlankOrNull(this.getValor()));
	}

	@Override
	public ValidatorError getError() {
		ValidatorError error = super.getError();
		if (error == null && !requiredValidation()) {
			error = new ValidatorError(this.getNome(), String.format("O campo %s é obrigatório", this.getNome()));
		}
		return error;
	}
}
