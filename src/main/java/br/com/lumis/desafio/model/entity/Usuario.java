package br.com.lumis.desafio.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of="id")
@Entity
public class Usuario implements EntityInterface{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotBlank
	@Size(min=3, max=255)
	@Column(length=255, nullable=false)
	private String nome;
	
	@Size(min=3, max=255)
	@Column(length=255, nullable=false, unique = true)
	private String username;
	
	@NotBlank
	@Size(min=64, max=64)
	@Column(length=64, nullable=false)
	private String senha;
	
	@ManyToOne
    @JoinColumn(name="perfil_fk", nullable=false)
	private Perfil perfil;

}
