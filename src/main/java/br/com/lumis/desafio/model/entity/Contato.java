package br.com.lumis.desafio.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Check;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Check(constraints = "estado IN ('RJ', 'SP')")
public class Contato implements EntityInterface {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Email
	@Column(length=255, nullable=false)
	private String email;

	@NotBlank
	@Size(min=3, max=255)
	@Column(length=255, nullable=false)
	private String nome;

	@NotBlank
	@Pattern(regexp = "\\(\\d{2}\\)\\s{0,1}[9]{0,1}\\d{4}-\\d{4}")
	@Column(length=15, nullable=false)
	private String telefone;

	@NotBlank
	@Size(min=2, max=2)
	@Column(length=2, nullable=false)
	private String estado;

	@NotBlank
	@Size(min=2, max=255)
	@Column(length=255, nullable=false)
	private String empresa;

	@NotBlank
	@Size(min=10, max=255)
	@Column(length=500, nullable=false)
	private String mensagem;
}
