package br.com.lumis.desafio.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Check;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@EqualsAndHashCode(of="id")
@Entity
@Check(constraints = "nome IN ('ADMINISTRATIVO', 'ATENDIMENTO')")
public class Perfil implements EntityInterface{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Size(min=3, max=255)
	@Column(length=255, nullable=false, unique = true)
	private String nome;

}
