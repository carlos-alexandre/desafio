package br.com.lumis.desafio.model.vo;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UsuarioVO implements Serializable {

	private static final long serialVersionUID = -8011984113127468911L;

	private Long id;
	private String nome;
	private String username;
	private String senha;
	private PerfilVO perfil;

}
