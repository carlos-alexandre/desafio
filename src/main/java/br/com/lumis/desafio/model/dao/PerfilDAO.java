package br.com.lumis.desafio.model.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.lumis.desafio.model.entity.Perfil;
import lombok.extern.log4j.Log4j;

@Log4j
public class PerfilDAO extends CRUDDAOBase<Perfil> {

	protected PerfilDAO(Class<Perfil> entityClass) {
		super(entityClass);
		// TODO Auto-generated constructor stub
	}

	public PerfilDAO() {
		this(Perfil.class);
	}


	public Perfil getPerfilPorNome(String nome) {
		super.loadEntityManager();
		String jql = "SELECT p FROM Perfil p WHERE p.nome = :nome";
		TypedQuery<Perfil> q = em.createQuery(jql, Perfil.class);
		try {
			return q.setParameter("nome", nome).getSingleResult();
		} catch (NoResultException e) {
			log.debug("Nenhum nome encontrado");
			log.debug(nome);
			return null;
		}
	}




	@Override
	protected Perfil doUpdate(Perfil entityOld, Perfil entity) {
		entityOld.setNome(entity.getNome());
		return entityOld;
	}

	@Override
	protected List<Perfil> doLoadAll() {
		String jql = "SELECT p FROM Perfil p";
		TypedQuery<Perfil> q = em.createQuery(jql, Perfil.class);
		return q.getResultList();

	}



}
