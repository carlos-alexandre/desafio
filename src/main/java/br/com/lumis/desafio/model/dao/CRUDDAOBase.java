package br.com.lumis.desafio.model.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.lumis.desafio.model.JPAUtil;
import br.com.lumis.desafio.model.entity.EntityInterface;

public abstract class CRUDDAOBase<T extends EntityInterface> {
	
	protected EntityManager em;
	final Class<T> entityClass;

    protected CRUDDAOBase(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
	
	public Long create(T entity) {
		this.loadEntityManager();
		em.getTransaction().begin();
		Long id = doCreate(entity); 
		em.getTransaction().commit();
		return id;
	}
	public T load(Long id) {
		this.loadEntityManager();
		return doLoad(id);
	}
	
	public List<T> loadAll() {
		this.loadEntityManager();
		return doLoadAll();
	}
	
	public T update(Long id, T entity) {
		this.loadEntityManager();
		T oldEntity = doLoad(id);
		return doUpdate(oldEntity, entity);
	}
	
	public void delete(Long id) {
		this.loadEntityManager();
		T entity = doLoad(id);
		em.getTransaction().begin();
		doDelete(entity); 
		em.getTransaction().commit();
	}
	
	protected Long doCreate(T entity) {
		em.persist(entity);
		return entity.getId();
	}
	protected void doDelete(T entity) {
		em.remove(entity);
	}

	protected abstract T doUpdate(T entityOld, T entity);

	protected abstract List<T> doLoadAll();

	protected T doLoad(Long id) {
		return em.find(entityClass, id);
	}
	
	protected void loadEntityManager() {
		this.em = JPAUtil.getEntityManager();
	}

}
