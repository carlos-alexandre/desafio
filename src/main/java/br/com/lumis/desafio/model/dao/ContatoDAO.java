package br.com.lumis.desafio.model.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import br.com.lumis.desafio.model.entity.Contato;

public class ContatoDAO extends CRUDDAOBase<Contato>{

	protected ContatoDAO(Class<Contato> entityClass) {
		super(entityClass);
	}
	
	public ContatoDAO() {
		this(Contato.class);
	}

	@Override
	protected Contato doUpdate(Contato entityOld, Contato entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<Contato> doLoadAll() {
		String jql = "SELECT p FROM Contato p";
		TypedQuery<Contato> q = em.createQuery(jql, Contato.class);
		return q.getResultList();
	}


	

}
