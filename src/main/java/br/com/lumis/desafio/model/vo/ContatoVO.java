package br.com.lumis.desafio.model.vo;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContatoVO implements Serializable{
	
	private static final long serialVersionUID = 4458898119184085524L;
	
	private Long id;
	private String email;
	private String nome;
	private String telefone;
	private String estado;
	private String empresa;
	private String mensagem;
}
