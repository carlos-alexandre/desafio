package br.com.lumis.desafio.model.vo;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class PerfilVO implements Serializable {

	private static final long serialVersionUID = 3951363224445645424L;
	
	private Long id;
	private String nome;

}
