package br.com.lumis.desafio.model.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.lumis.desafio.model.entity.Usuario;
import lombok.extern.log4j.Log4j;

@Log4j
public class UsuarioDAO extends CRUDDAOBase<Usuario>{

	protected UsuarioDAO(Class<Usuario> entityClass) {
		super(entityClass);
		// TODO Auto-generated constructor stub
	}
	
	public UsuarioDAO() {
		super(Usuario.class);
	}

	@Override
	protected Usuario doUpdate(Usuario entityOld, Usuario entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<Usuario> doLoadAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Usuario getUsuarioPorUsername(String username) {
		super.loadEntityManager();
		String jql = "SELECT u FROM Usuario u WHERE u.username = :username";
		TypedQuery<Usuario> q = em.createQuery(jql, Usuario.class);
		try {
			return q.setParameter("username", username).getSingleResult();
		} catch (NoResultException e) {
			log.debug("Nenhum username encontrado");
			log.debug(username);
			return null;
		}
	}

}
