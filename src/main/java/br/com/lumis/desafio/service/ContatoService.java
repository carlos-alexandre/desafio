package br.com.lumis.desafio.service;

import java.util.List;

import br.com.lumis.desafio.model.dao.ContatoDAO;
import br.com.lumis.desafio.model.entity.Contato;
import br.com.lumis.desafio.model.vo.ContatoVO;

public class ContatoService extends ServiceBase<Contato, ContatoVO>{
	private ContatoDAO contadoDao;

	public ContatoService(ContatoDAO contadoDao) {
		this.contadoDao = contadoDao;
	}

	public List<ContatoVO> getAll() {
		return this.entityListToVOList(
				this.contadoDao.loadAll()
		);
	}
	
	public ContatoVO getContado(Long id) {
		Contato contato = this.contadoDao.load(id);
		if (contato == null)
			return null;
		return entityToVO(contato);
	}
	
	public void createContato(ContatoVO vo) {
		vo.setId(null);
		Long id = this.contadoDao.create(VOtoEntity(vo));
		vo.setId(id);
	}
	
	public void deleteContato(Long id) {
		this.contadoDao.delete(id);
	}

	@Override
	public ContatoVO entityToVO(Contato entity) {
		ContatoVO vo = new ContatoVO();
		vo.setNome(entity.getNome());
		vo.setEmail(entity.getEmail());
		vo.setEmpresa(entity.getEmpresa());
		vo.setEstado(entity.getEstado());
		vo.setMensagem(entity.getMensagem());
		vo.setTelefone(entity.getTelefone());
		vo.setId(entity.getId());
		return vo;
	}

	@Override
	public Contato VOtoEntity(ContatoVO vo) {
		Contato entity = new Contato();
		entity.setNome(vo.getNome());
		entity.setEmail(vo.getEmail());
		entity.setEmpresa(vo.getEmpresa());
		entity.setEstado(vo.getEstado());
		entity.setMensagem(vo.getMensagem());
		entity.setTelefone(vo.getTelefone());
		entity.setId(vo.getId());
		return entity;

	}

	

}
