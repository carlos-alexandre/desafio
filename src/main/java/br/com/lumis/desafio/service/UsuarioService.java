package br.com.lumis.desafio.service;

import br.com.lumis.desafio.model.dao.PerfilDAO;
import br.com.lumis.desafio.model.dao.UsuarioDAO;
import br.com.lumis.desafio.model.entity.Perfil;
import br.com.lumis.desafio.model.entity.Usuario;
import br.com.lumis.desafio.model.vo.PerfilVO;
import br.com.lumis.desafio.model.vo.UsuarioVO;

public class UsuarioService extends ServiceBase<Usuario, UsuarioVO> {
	private UsuarioDAO usuarioDao;
//	private PerfilDAO perfilDao;

	public UsuarioService(UsuarioDAO usuarioDao) {
		this.usuarioDao = usuarioDao;
	}

//	public UsuarioService(UsuarioDAO usuarioDao, PerfilDAO perfilDao) {
//		this(usuarioDao);
//		this.perfilDao = perfilDao;
//	}

	public UsuarioVO getUsuario(Long id) {
		Usuario Usuario = this.usuarioDao.load(id);
		if (Usuario == null)
			return null;
		return entityToVO(Usuario);
	}

	public UsuarioVO getUsuarioPorUsername(String username) {
		Usuario Usuario = this.usuarioDao.getUsuarioPorUsername(username);
		if (Usuario == null)
			return null;
		return entityToVO(Usuario);
	}

	@Override
	public UsuarioVO entityToVO(Usuario entity) {
		UsuarioVO vo = new UsuarioVO();
		vo.setNome(entity.getNome());
		vo.setSenha(entity.getSenha());
		vo.setUsername(entity.getUsername());
		vo.setId(entity.getId());

		vo.setPerfil(getPerfilVO(entity.getPerfil()));

		return vo;
	}

	@Override
	public Usuario VOtoEntity(UsuarioVO vo) {
		Usuario entity = new Usuario();
		entity.setNome(vo.getNome());
		entity.setSenha(vo.getSenha());
		entity.setUsername(vo.getUsername());
		entity.setId(vo.getId());

		entity.setPerfil(getPerfilEntity(vo.getPerfil()));
		return entity;

	}

	public PerfilVO getPerfilVO(Perfil entity) {
		PerfilVO vo = new PerfilVO();
		vo.setId(entity.getId());
		vo.setNome(entity.getNome());
		return vo;
	}

	public Perfil getPerfilEntity(PerfilVO vo) {
		Perfil entity = new Perfil();
		entity.setId(vo.getId());
		entity.setNome(vo.getNome());
		return entity;
	}

}
