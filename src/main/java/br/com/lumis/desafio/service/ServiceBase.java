package br.com.lumis.desafio.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.lumis.desafio.model.entity.EntityInterface;

public abstract class ServiceBase<E extends EntityInterface, VO> {
	public List<VO> entityListToVOList(List<E> entities) {
		return entities.stream().map(entity -> this.entityToVO(entity))
				.collect(Collectors.toCollection(LinkedList<VO>::new));
	}

	public List<E> VOListToEntityList(List<VO> vos) {
		return vos.stream().map(vo -> this.VOtoEntity(vo))
				.collect(Collectors.toCollection(LinkedList<E>::new));
	}

	protected abstract VO entityToVO(E entity);
	protected abstract E VOtoEntity(VO vo);

}
