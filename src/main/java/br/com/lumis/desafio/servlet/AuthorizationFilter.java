package br.com.lumis.desafio.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.map.HashedMap;

import br.com.lumis.desafio.model.vo.PerfilVO;
import br.com.lumis.desafio.model.vo.UsuarioVO;
import lombok.extern.log4j.Log4j;

@Log4j
@WebFilter(asyncSupported = true, urlPatterns = { "/*" })
public class AuthorizationFilter implements Filter {

	private Map<String, String> protectedResource;

	@Override
	@SuppressWarnings("unchecked")
	public void init(FilterConfig filterConfig) throws ServletException {
		Filter.super.init(filterConfig);
		this.protectedResource = new HashedMap();

		String contextPath = filterConfig.getServletContext().getContextPath();

		this.protectedResource.put(contextPath + "/contatos.jsp", "GET");
		this.protectedResource.put(contextPath + "/contatos", "GET");

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain next)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		String requestedUri = req.getRequestURI();

		if (protectedResource.containsKey(requestedUri)) {
			String method = req.getMethod();
			if (method.equalsIgnoreCase(protectedResource.get(requestedUri))) {
				HttpSession session = req.getSession(false);
				if (session == null) {
					sendErro(req, resp);
					return;
				}
				UsuarioVO usuario = (UsuarioVO) session.getAttribute("usuario");
				if (usuario == null || usuario.getPerfil() == null) {
					sendErro(req, resp);
					return;
				}
				PerfilVO perfil = usuario.getPerfil();
				if (!perfil.getNome().equalsIgnoreCase("ADMINISTRATIVO") &&
				    !perfil.getNome().equalsIgnoreCase("ATENDIMENTO")) {
					sendErro(req, resp);
					return;
				}
			}
		}
		next.doFilter(request, response);
	}

	private void sendErro(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.warn("Acesso não autorizado");
		HttpSession session = req.getSession(true);
		session.setAttribute("error", "Accesso negado, faça o login");
		resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
		resp.sendRedirect(req.getContextPath() + "/login.jsp");
	}
}