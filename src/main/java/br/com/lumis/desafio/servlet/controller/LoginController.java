package br.com.lumis.desafio.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;

import br.com.lumis.desafio.model.dao.UsuarioDAO;
import br.com.lumis.desafio.model.vo.UsuarioVO;
import br.com.lumis.desafio.service.UsuarioService;
import br.com.lumis.desafio.validator.BaseValidator;
import br.com.lumis.desafio.validator.RequiredValidator;
import br.com.lumis.desafio.validator.error.ValidatorError;
import lombok.extern.log4j.Log4j;

/**
 * Servlet implementation class ContatoList
 */
@WebServlet("/login")
@Log4j
public class LoginController extends HttpServlet {

	private static final long serialVersionUID = -8892256330272957017L;

	private UsuarioService service;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		this.service = new UsuarioService(new UsuarioDAO());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Verificando os campos username e senha não estão vazios
		if (this.validateForm(req) != null) {
			this.sendError(req, resp);
			return;
		}

		// Invalidando a sessão antiga
		HttpSession oldSession = req.getSession(false);
		if (oldSession != null) {
			oldSession.invalidate();
		}

		String username = req.getParameter("username");
		String senha = req.getParameter("senha");

		// Verificando o username no bd
		UsuarioVO u = this.service.getUsuarioPorUsername(username);
		if (u == null) {
			// Username não encontrado
			this.sendError(req, resp);
			return;
		}

		// Validando a senha criptografada
		if (!DigestUtils.sha256Hex(senha).equals(u.getSenha())) {
			// Senhas diferentes
			this.sendError(req, resp);
			return;
		}

		// Login válido

		log.debug("Login válido");

		// criando uma nova sessão
		HttpSession newSession = req.getSession(true);

		// a sessão expira em 15min
		newSession.setMaxInactiveInterval(15 * 60);

		// Adicionando o usuario a sessão
		newSession.setAttribute("usuario", u);
		
		resp.sendRedirect(req.getContextPath() + "/contatos.jsp");

	}

	private ValidatorError validateForm(HttpServletRequest req) {
		RequiredValidator usernameValidator = new RequiredValidator(
				new BaseValidator("username", req.getParameter("username")));

		if (!usernameValidator.validate())
			return usernameValidator.getError();

		RequiredValidator senhaValidator = new RequiredValidator(new BaseValidator("senha", req.getParameter("senha")));

		if (!senhaValidator.validate())
			return senhaValidator.getError();

		return null;
	}

	private void sendError(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.warn("Login ou senha inválidos");
		req.setAttribute("error", "Login ou senha inválidos");
		req.getRequestDispatcher("/login.jsp").forward(req, resp);
	}
}
