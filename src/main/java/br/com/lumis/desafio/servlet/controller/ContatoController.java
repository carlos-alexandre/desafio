package br.com.lumis.desafio.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import br.com.lumis.desafio.model.dao.ContatoDAO;
import br.com.lumis.desafio.model.vo.ContatoVO;
import br.com.lumis.desafio.model.vo.PerfilVO;
import br.com.lumis.desafio.model.vo.UsuarioVO;
import br.com.lumis.desafio.service.ContatoService;
import br.com.lumis.desafio.validator.BaseValidator;
import br.com.lumis.desafio.validator.EmailValidator;
import br.com.lumis.desafio.validator.LengthRangeValidator;
import br.com.lumis.desafio.validator.RequiredValidator;
import br.com.lumis.desafio.validator.TelefoneValidator;
import br.com.lumis.desafio.validator.ValuesValidator;
import br.com.lumis.desafio.validator.error.ValidatorError;
import lombok.extern.log4j.Log4j;

/**
 * Servlet implementation class ContatoList
 */
@WebServlet(asyncSupported=true, urlPatterns= {"/contatos"})
@Log4j
public class ContatoController extends HttpServlet {
	private static final long serialVersionUID = -1035818704464181157L;

	private ContatoService service;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ContatoController() {
		super();
		this.service = new ContatoService(new ContatoDAO());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UsuarioVO usuario = (UsuarioVO) req.getSession().getAttribute("usuario");

		if (usuario == null) {
			this.sendError(req, resp, "Acesso não autorizado", "/login.jsp");
			return;
		}
		
		String action = req.getParameter("action");
		if (action == null) {
			req.getRequestDispatcher("/contatos.jsp").forward(req, resp);
		} else if (action.equalsIgnoreCase("list")) {
			List<ContatoVO> contatos = service.getAll();
			Gson gson = new Gson();
			sendJsonResponse(resp, gson.toJson(contatos));
		} else if (action.equalsIgnoreCase("remove")) {
			PerfilVO p = usuario.getPerfil();
			if (!p.getNome().equalsIgnoreCase("ADMINISTRATIVO")) {
				log.warn("Operação não autorizada");
				Gson gson = new Gson();
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				sendJsonResponse(resp, gson.toJson(new ValidatorError("error", "Operação não autorizada")));
				return;
			}
			String[] ids = req.getParameterValues("ids[]");
			for (int i = 0; i < ids.length; i++) {
				service.deleteContato(Long.valueOf(ids[i]));
			}
		}
	}

	

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ValidatorError error = validateRequest(req);
		if (error != null) {
			Gson gson = new Gson();
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			sendJsonResponse(resp, gson.toJson(error));
			return;
		}
		ContatoVO contatoVO = new ContatoVO();

		contatoVO.setEmail(req.getParameter("email").trim());
		contatoVO.setEmpresa(req.getParameter("empresa").trim());
		contatoVO.setEstado(req.getParameter("estado").trim());
		contatoVO.setMensagem(req.getParameter("mensagem").trim());
		contatoVO.setTelefone(req.getParameter("telefone").trim());
		contatoVO.setNome(req.getParameter("nome").trim());

		service.createContato(contatoVO);
		req.getRequestDispatcher("/contato.jsp").forward(req, resp);
	}

	private void sendJsonResponse(HttpServletResponse resp, String json) throws IOException {
		PrintWriter out = resp.getWriter();
		resp.setContentType("application/json;charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		out.print(json);
		out.flush();
	}
	
	private void sendError(HttpServletRequest req, HttpServletResponse resp, String errorMsg, String target) throws IOException {
		log.warn(errorMsg);
		HttpSession session = req.getSession(true);
		session.setAttribute("error", errorMsg);
		resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
		resp.sendRedirect(req.getContextPath() + target);
	}

	private ValidatorError validateRequest(HttpServletRequest req) {
		EmailValidator emailValidator = new EmailValidator(
				new RequiredValidator(new BaseValidator("email", req.getParameter("email"))));

		if (!emailValidator.validate())
			return emailValidator.getError();

		TelefoneValidator telefoneValidator = new TelefoneValidator(
				new RequiredValidator(new BaseValidator("telefone", req.getParameter("telefone"))));

		if (!telefoneValidator.validate())
			return telefoneValidator.getError();

		LengthRangeValidator empresaValidator = new LengthRangeValidator(2, 255,
				new RequiredValidator(new BaseValidator("empresa", req.getParameter("empresa"))));

		if (!empresaValidator.validate())
			return empresaValidator.getError();

		LengthRangeValidator nomeValidator = new LengthRangeValidator(3, 255,
				new RequiredValidator(new BaseValidator("nome", req.getParameter("nome"))));

		if (!nomeValidator.validate())
			return nomeValidator.getError();

		LengthRangeValidator mensagemValidator = new LengthRangeValidator(10, 500,
				new RequiredValidator(new BaseValidator("mensagem", req.getParameter("mensagem"))));

		if (!mensagemValidator.validate())
			return mensagemValidator.getError();

		ValuesValidator estadoValidator = new ValuesValidator("RJ, SP",
				new RequiredValidator(new BaseValidator("estado", req.getParameter("estado"))));

		if (!estadoValidator.validate())
			return estadoValidator.getError();

		return null;
	}

}
