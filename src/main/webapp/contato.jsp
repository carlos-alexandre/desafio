
<jsp:include page="/includes/header.jsp"/>
<link rel="stylesheet" href="css/parsley.css"/>
<section class="callout">
            <div class="site-logo"></div>
            <h1>Contato</h1>
        </section>
    </header>
    <section class="row internal-content">
        <article class="all-full">
            <ul class="breadcrumb">
                <li><a href="index.jsp">Home</a></li>
                <li>Contato</li>
            </ul>
        </article>
        <article class="news-detail desktop-8 desktop-push-2 tablet-full mobile-full">
            <h1>Entre em contato com a gente.</h1>
            <p class="intro">Preencha o formul�rio abaixo. Todos os campos s�o obrigat�rios.</p>
            <div class="row form">
                <form id="myForm"  method="post" action="contatos">
					<div class="desktop-6 tablet-3 mobile-full">
						<label for="nome">Nome:</label>
						<input id="nome" name="nome" type="text" class="input" minlength="3" maxlength="255" required/>
					</div>
					<div class="desktop-6 tablet-3 mobile-full">
						<label for="email">E-mail:</label>
						<input id="email" name="email" type="email" class="input" required/>
					</div>
					<div class="clear"></div>
					<div class="desktop-4 tablet-2 mobile-full">
						<label for="telefone">Telefone:</label>
						<input id="telefone" name="telefone" type="text" class="input formatphone" 
						placeholder="(99) 99999-9999"
						data-parsley-length="[14, 15]"
						required />
					</div>
					<div class="desktop-2 tablet-1 mobile-full">
						<label for="estado">Estado:</label>
						<select id="estado" name="estado" class="input" required>
							<option value="RJ">RJ</option>
							<option value="SP">SP</option>
						</select>
					</div>
					<div class="desktop-6 tablet-3 mobile-full">
						<label for="empresa">Empresa:</label>
						<input id="empresa" name="empresa" type="text" class="input" minlength="2" maxlength="255" required/>
					</div>
					<div class="all-full">
						<label for="mensagem">Mensagem:</label>
						<textarea id="mensagem" name="mensagem" class="input" required minlength="10" maxlength="500"></textarea>
					</div>
					<div class="all-full">
						<button id="enviar" class="button-primary">Enviar</button>
					</div>
				</form>
           </div>
        </article>
    </section>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/parsley.min.js"></script>
    <script src="js/i18n/parsley-pt-br.js"></script>
     
    <script type="text/javascript">	
		$(document).ready(function() {
			$("input, textarea, select").parsley({
				 trigger: "focusout input"
			});
			
			var myForm = $("#myForm").parsley();
			
			Parsley.on('form:submit', function() {
				$.ajax({
					"url": "contatos",
					"type": "POST",
					"data": $("#myForm").serialize(),
					"success": function(result, status, xhr){
						alert("Dados enviados com sucesso");
						$("#myForm").trigger("reset");
						myForm.reset();
					},
					"error": function(xhr, status, error){
						var err = $.parseJSON(xhr.responseText);
						var errElement = $("#"+err.element);
						errElement.parsley().reset();
						errElement.parsley().addError('forcederror', {message: err.mensage, updateClass: true});
						console.error(err);
						alert(err.mensage);
					}
				});
			    return false;
			  });
			 
			$("#telefone").mask("(99) 9999-9999?9")
			    .focusout(function (event) {  
		            var target, phone, element;  
		            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
		            phone = target.value.replace(/\D/g, '');
		            element = $(target);  
		            element.unmask();  
		            if(phone.length > 10) {  
		                element.mask("(99) 99999-999?9");  
		            } else {  
		                element.mask("(99) 9999-9999?9");  
		            }  
		        });
		});
	</script>
    <jsp:include page="/includes/footer.jsp"/>
    
    