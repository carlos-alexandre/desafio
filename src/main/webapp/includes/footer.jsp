<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<footer class="site-footer">
        <section class="row">
            <article class="desktop-3 tablet-3 mobile-full">
                <h1>Sobre a Lumis</h1>
                <p>Pioneira no mercado de portais com a plataforma Lumis Portal, a Lumis oferece produtos e serviços que apoiam seus clientes em múltiplos canais digitais.</p>
                <p><a href="www.lumis.com">www.lumis.com.br</a></p>
            </article>
            <article class="desktop-3 tablet-3 mobile-full">
                <h1>Sobre o Lumis Portal</h1>
                <p>Utilizado em projetos críticos nas maiores empresas do país, o Lumis Portal destaca-se como  a primeira plataforma 100% brasileira para gestão portais.</p>
                <p><a href="www.lumis.com">www.lumisportal.com.br</a></p>
            </article>
            <article class="desktop-3 tablet-3 mobile-full social">
                <h1>Redes Sociais</h1>
                <div><i class="fa fa-fw fa-facebook"></i><a href="#">Facebook</a></div>
                <div><i class="fa fa-fw fa-linkedin"></i><a href="#">Linked In</a></div>
                <div><i class="fa fa-fw fa-twitter"></i><a href="#">Twitter</a></div>
                <div><i class="fa fa-fw fa-youtube"></i><a href="#">Youtube</a></div>
            </article>
            <article class="desktop-3 tablet-3 mobile-full">
                <h1 class="logo-lumis">Lumis</h1>
                <p>Powered by <span class="powered">Lumis Portal</span></p>
            </article>
        </section>
    </footer>
    
    <script src="js/jquery.slicknav.min.js"></script>
    <script>
        $(function(){
            $('#menu').slicknav();
        });
    </script>
</body>
</html>