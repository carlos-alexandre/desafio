<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Treinamento Lumis Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <base href="${pageContext.request.contextPath}/" />
    <link rel="stylesheet" href="css/reset.css"/>
    <link rel="stylesheet" href="css/gridlock/fs.gridlock.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/slicknav.css"/>
    <link rel="stylesheet" href="css/main.css"/>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>-->
    <script src="js/jquery/jquery-1.11.1.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'/>
</head>
<body class="gridlock">
    <header class="main-header">
        <nav id="menu" class="navigation">
            <ul class="row">
                <li class="desktop-2 tablet-1 mobile-full"><a class="navigation-link active " href="index.jsp">Home</a></li>
                <li class="desktop-2 tablet-1 mobile-full"><a class="navigation-link " href="contato.jsp">Contato</a></li>
                <c:choose>
	                <c:when test="${usuario != null}">
	                <li class="desktop-2 tablet-1 mobile-full"><a class="navigation-link " href="contatos.jsp">Lista de Contatos</a></li>
	                <li class="desktop-2 tablet-1 mobile-full"><a class="navigation-link " href="logout">Logout</a></li>
	                </c:when>
	                <c:otherwise>
	                <li class="desktop-2 tablet-1 mobile-full"><a class="navigation-link " href="login.jsp">Login</a></li>
	                </c:otherwise>
                </c:choose>
            </ul>
        </nav>
        
        <c:choose>
  <c:when test="${usuario != null}">
    ...
  </c:when>
  <c:when test="${condition2}">
    ...
  </c:when>
  <c:otherwise>
    ...
  </c:otherwise>
</c:choose>
        