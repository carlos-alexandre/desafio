<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/includes/header.jsp" />
<link rel="stylesheet"
	href="css/jquery.dataTables.min.css" />
<link rel="stylesheet"
	href="css/responsive.dataTables.min.css" />
<link rel="stylesheet"
	href="css/select.dataTables.min.css" />
<link rel="stylesheet"	
	href="css/buttons.dataTables.min.css" />

<section class="callout">
	<div class="site-logo"></div>
	<h1>Administrativo</h1>
</section>
</header>
<section class="row internal-content">
	<article class="all-full">
		<ul class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Administrativo</li>
		</ul>
	</article>
	<article class="news-detail desktop-8 desktop-push-2 tablet-full mobile-full">
	    <h1>Lista de Contatos</h1>
	    <p class="intro">Clique no �cone verde para ver a mensagem do contado</p>
	    <div id="error" class="error"></div>
	    
	    <div class="row form">
			<table id="contatos" class="display responsive no-wrap" style="width: 100%">
							
			</table>
			<c:if test="${usuario.perfil.nome == 'ADMINISTRATIVO'}">
			<button id="remover">Remover Selecionados</button>
			</c:if>
			<button id="dselect">Limpar Sele��o</button>
		</div>
	</article>
</section>

<script
	src="js/jquery.dataTables.min.js"></script>
<script
	src="js/dataTables.responsive.min.js"></script>
<script
	src="js/dataTables.select.min.js"></script>

<script>	
	$(document).ready(function() {
		var table = $('#contatos').DataTable({
			"ajax": {
				"url": "contatos?action=list",
				"dataSrc": "",
			},
			"columns": [
		        { "title": "Nome", data: 'nome',  className: "all" },
		        { "title": "E-mail", data: 'email' },
		        { "title": "Telefone", data: 'telefone' },
		        { "title": "Estado", data: 'estado' },
		        { "title": "Empresa", data: 'empresa' },
		        { "title": "Mensagem", data: 'mensagem',  className: "none"},
		        { "title": "id", data: 'id', visible: false},
		        null
		    ],
		    
		    "select": {
	          "style": "multi"
	        },
	        responsive: {
	            details: {
	                type: 'column',
	                target: -1,
	                renderer: function ( api, rowIdx, columns ) {
	                    var data = $.map( columns, function ( col, i ) {
	                        return col.hidden ?
	                        	'<span class="title">'+col.title+':</span><br>'+
	                        	'<span calss="value">'+col.data+'</span>'+
	                        	'<hr>' :
	                            '';
	                    }).join('');
	 
	                    return data ?
	                        $('<div class="hidden-details"/>').append( data ) :
	                        false;
	                }
	            }
	        },
	        columnDefs: [ {
	            className: 'control',
	            "defaultContent": "",
	            orderable: false,
	            targets:   -1
	        } ],
			"language": {
			    "decimal":        ",",
			    "emptyTable":     "Nenhum Contado Adicionado",
			    "info":           "Listando do _START_ at� _END_ do total de _TOTAL_ contatos",
			    "infoEmpty":      "",
			    "infoFiltered":   "",
			    "infoPostFix":    "",
			    "thousands":      ".",
			    "lengthMenu":     "Listar _MENU_ contatos",
			    "loadingRecords": "Carregando...",
			    "processing":     "Processando...",
			    "search":         "Procurar:",
			    "zeroRecords":    "Nenhum contato encontrado",
			    "paginate": {
			        "first":      "primeiro",
			        "last":       "�ltimo",
			        "next":       "pr�ximo",
			        "previous":   "anterior"
			    },
			    "select": {
		            "cells": "",
		            "rows": "",
		            "columns": ""
		        },
			    "aria": {
			        "sortAscending":  ": ordenar a coluna",
			        "sortDescending": ": ordenar a coluna (inversa)"
			    }
			}
		});
		$("#dselect").click(function() {
			table.rows().deselect();
		});
		
		<c:if test="${usuario.perfil.nome == 'ADMINISTRATIVO'}">
		$("#remover").click(function() {
			var deletedids = [];
        	var rows = table.rows( { selected: true } );
        	var data = rows.data().toArray();
        	data.forEach(function(value){
        		deletedids.push(value.id);
        	});
        	if (deletedids.length > 0) {
	        	$.ajax({
	        		"url": "contatos?action=remove", 
	        		"data": {"ids": deletedids},
	   				"success": function(result){
	   					$("#error").text("");
	   					rows.remove().draw(false);
	   					alert("Contatos removidos com sucesso")
	            	},
	            	"error": function(xhr, status, error){
						var err = $.parseJSON(xhr.responseText);
						$("#error").text(err.mensage);
						console.error(err);
						alert(err.mensage);
					}
	        	});
        	}
		});
		</c:if>
	});
</script>

<jsp:include page="/includes/footer.jsp" />