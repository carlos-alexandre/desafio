<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/includes/header.jsp" />
<section class="callout">
	<div class="site-logo"></div>
	<h1>Login</h1>
</section>
</header>
<section class="row internal-content">
	<article class="all-full">
		<ul class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li>Login</li>
		</ul>
	</article>
	<article
		class="news-detail desktop-8 desktop-push-2 tablet-full mobile-full">
		<h1>Login</h1>

		<c:if test="${error != null}">
		 <div id="error" class="error">      
   			<c:out value="${error}" />
   		</div>
		</c:if>


		<div class="row form">
			<form id="login" action="login" method="post">
				<div class="desktop-6 tablet-1 mobile-full">
					<label for="username" >Login:</label> 
					<input id="username" name="username" type="text" class="input">
				</div>

				<div class="clear"></div>


				<div class="desktop-6 tablet-3 mobile-full">
					<label for="senha">Senha: </label> 
					<input id="senha" name="senha" type="password"class="input">
				</div>

				<div class="all-full">
					<button class="button-primary">Enviar</button>
				</div>
			</form>
		</div>
	</article>
</section>

<jsp:include page="/includes/footer.jsp" />