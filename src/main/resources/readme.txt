
O objetivo do desafio é avaliar o nível de conhecimento do candidato nas seguintes áreas:   
•	Banco de Dados;  
•	Desenvolvimento em Java (JEE, Tomcat, Hibernate, JAX-RS); 
•	Desenvolvimento Web (Ajax, JQuery, Javascript, HTML, CSS). 

O candidato deve implementar uma aplicação Java WEB, sem a utilização de frameworks MVC (Spring, JSF, etc);

O candidato deve enviar o arquivo .war com as instruções para o deploy da aplicação

DESAFIO:
	A Lumis disponibilizará um HTML com as seguintes páginas:
		Index.html
		Login.html
		Contato.html

Página Inicial
•	A aplicação deve apresentar a página inicial com o mesmo layout do arquivo index.html
•	A página inicial deve conter 2 links:  Contato e Login;
Contato
•	A página de contado deve exibir o formulário com o mesmo layout do arquivo contato.html;
•	Ao preencher e submeter o formulário, os dados devem ser armazenados no banco de Dados e uma mensagem de sucesso deve ser apresentada na tela;
•	Todos os campos do formulário são de preenchimento obrigatório e devem ser validados;
Login
•	A página de login deve exibir o formulário com o mesmo layout do arquivo Login.html;
•	Ao preencher e submeter o formulário, os dados de acesso devem ser validados e o usuário deve ser redirecionado para área restrita do site;
Área restrita
•	Ao se logar na aplicação o usuário deve ser redirecionado para a página “Área restrita”;
•	Ao se logar na aplicação deve ser exibido no Menu o Link para área restrita e o Link de Login não deverá mais estar disponível;
•	A página “Área restrita” só deve ser acessível quando o usuário estiver autenticado no site.
•	A página “Área restrita” deve exibir a lista de contatos armazenados no banco de dados de acordo com o perfil de acesso
Perfil de Acesso
•	Deverá ser considerado 2 perfis de acesso ao site;
•	Atendente e administrador
o	Atendente: Visualiza a lista de contatos preenchidos no site
o	Administrador: Visualiza e pode excluir os contatos preenchidos no site







