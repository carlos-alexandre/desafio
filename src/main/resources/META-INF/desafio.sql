PRAGMA foreign_keys=OFF;

DELETE FROM hibernate_sequence;
INSERT INTO hibernate_sequence SELECT COALESCE(MAX(id), 0)+1 FROM Contato;

DELETE FROM Perfil;
INSERT INTO Perfil (id, nome) SELECT next_val, 'ADMINISTRATIVO' FROM hibernate_sequence;
UPDATE hibernate_sequence SET next_val = next_val + 1;

INSERT INTO Perfil (id, nome) SELECT next_val, 'ATENDIMENTO' FROM hibernate_sequence;
UPDATE hibernate_sequence SET next_val = next_val + 1;

DELETE FROM Usuario;
INSERT INTO Usuario (id,nome,senha,username,perfil_fk) SELECT next_val,'Administrador','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','admin', 0 FROM hibernate_sequence;
UPDATE Usuario set perfil_fk = (SELECT id from Perfil WHERE nome='ADMINISTRATIVO') WHERE username = 'admin';
UPDATE hibernate_sequence SET next_val = next_val + 1;

INSERT INTO Usuario (id,nome,senha,username,perfil_fk) SELECT next_val,'Atendente','2577809a528e6307987d97be7a33bc149c9208d45ade553892182e9c0f418e30','atend', 0 FROM hibernate_sequence;
UPDATE Usuario set perfil_fk = (SELECT id from Perfil WHERE nome='ATENDIMENTO') WHERE username = 'atend';
UPDATE hibernate_sequence SET next_val = next_val + 1;

DELETE FROM Contato WHERE email='carlos.alexandre@outlook.com';
INSERT INTO Contato (email, empresa, estado, mensagem, nome, telefone, id) SELECT 'carlos.alexandre@outlook.com', 'Lumis', 'RJ', 'Desafio Lumis', 'Carlos Alexandre S. da Fonseca', '(21) 99953-6603', next_val FROM hibernate_sequence;
UPDATE hibernate_sequence SET next_val = next_val + 1;
