CREATE TABLE IF NOT EXISTS hibernate_sequence (next_val bigint);

CREATE TABLE IF NOT EXISTS Contato (id bigint not null, email varchar(255) not null, empresa varchar(255) not null, estado varchar(2) not null, mensagem varchar(500) not null, nome varchar(255) not null, telefone varchar(15) not null, primary key (id), check (estado IN ('RJ', 'SP')));

CREATE TABLE IF NOT EXISTS Perfil (id bigint not null, nome varchar(255) not null unique, primary key (id), check (nome IN ('ADMINISTRATIVO', 'ATENDIMENTO')));

CREATE TABLE IF NOT EXISTS Usuario (id bigint not null, nome varchar(255) not null, senha varchar(64) not null, username varchar(255) not null unique, perfil_fk bigint not null, primary key (id), foreign key(perfil_fk) references perfil(id));
