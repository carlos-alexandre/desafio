package br.com.lumis.desafio.model.dao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.lumis.desafio.model.entity.Perfil;
import br.com.lumis.desafio.model.entity.Usuario;
import junit.framework.TestCase;

public class UsuarioDAOTest extends TestCase {

	UsuarioDAO dao;
	Usuario u1;
	Usuario u2;
	Long idU1;
	Long idU2;

	@Before
	protected void setUp() throws Exception {
		PerfilDAO pDao = new PerfilDAO();
		dao = new UsuarioDAO();
		u1 = new Usuario();
		u1.setNome("teste1");
		u1.setUsername("teste1");
		u1.setSenha("teste1");
		u1.setPerfil(pDao.getPerfilPorNome("ADMINISTRATIVO"));

		u2 = new Usuario();
		u2.setNome("teste2");
		u2.setUsername("teste2");
		u2.setSenha("teste2");
		u2.setPerfil(pDao.getPerfilPorNome("ATENDIMENTO"));

		idU1 = dao.create(u1);
		idU2 = dao.create(u2);

	}

	@After
	protected void tearDown() throws Exception {
		if (idU1 != null) {
			dao.delete(idU1);
		}

		if (idU2 != null) {
			dao.delete(idU2);
		}

	}

	

	@Test
	public void testLoad() throws Exception {
		Usuario p = dao.load(idU1);
		assertThat(p).isEqualTo(u1);

		p = dao.load(idU2);
		assertThat(p).isEqualTo(u2);

		p = dao.load(-1l);
		assertThat(p).isNull();

	}
	
	@Test
	public void testGetUsuarioPorUsername() {
		Usuario p = dao.getUsuarioPorUsername("teste1");
		assertThat(p).isEqualTo(u1);

		p = dao.getUsuarioPorUsername("teste2");
		assertThat(p).isEqualTo(u2);

		p = dao.getUsuarioPorUsername("nome3");
		assertThat(p).isNull();
	}

	

}
