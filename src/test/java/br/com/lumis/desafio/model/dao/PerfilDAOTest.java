package br.com.lumis.desafio.model.dao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.lumis.desafio.model.entity.Perfil;
import junit.framework.TestCase;

public class PerfilDAOTest extends TestCase {

	PerfilDAO dao;
	

	@Before
	protected void setUp() throws Exception {
		dao = new PerfilDAO();
		
	}
	
	

	@Test
	public void testGetPerfilPorNome() {
		Perfil p = dao.getPerfilPorNome("ADMINISTRATIVO");
		assertThat(p).isNotNull();
		assertThat(p.getNome()).isEqualTo("ADMINISTRATIVO");

		p = dao.getPerfilPorNome("ATENDIMENTO");
		assertThat(p).isNotNull();
		assertThat(p.getNome()).isEqualTo("ATENDIMENTO");

		p = dao.getPerfilPorNome("nome3");
		assertThat(p).isNull();
	}

	@Test
	public void testLoad() throws Exception {
		Perfil pAdm = dao.getPerfilPorNome("ADMINISTRATIVO");
		Perfil p = dao.load(1l);
		assertThat(p).isEqualTo(pAdm);

		Perfil pAten = dao.getPerfilPorNome("ATENDIMENTO");
		p = dao.load(2l);
		assertThat(p).isEqualTo(pAten);

		p = dao.load(-1l);
		assertThat(p).isNull();

	}

}
