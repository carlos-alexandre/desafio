package br.com.lumis.desafio.model.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Arrays.array;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.lumis.desafio.model.entity.Contato;
import junit.framework.TestCase;

public class ContatoDAOTest extends TestCase {

	ContatoDAO dao;
	Contato c1;
	Contato c2;
	Long idC1;
	Long idC2;

	@Before
	protected void setUp() throws Exception {
		dao = new ContatoDAO();
		
		//Limpando a tabela de contatos antes dos testes
		this.clearContatos();
		
		
		c1 = new Contato();
		c1.setNome("teste1");
		c1.setEmail("teste1@teste.com");
		c1.setEmpresa("teste1");
		c1.setEstado("RJ");
		c1.setTelefone("(21) 97654-3210");
		c1.setMensagem("teste1");

		c2 = new Contato();
		c2.setNome("teste2");
		c2.setEmail("teste2@teste.com");
		c2.setEmpresa("teste2");
		c2.setEstado("SP");
		c2.setTelefone("(11) 2654-3210");
		c2.setMensagem("teste2");

		idC1 = dao.create(c1);
		idC2 = dao.create(c2);

	}

	@After
	protected void tearDown() throws Exception {
		this.clearContatos();
	}

	@Test
	public void testLoadAll() {
		List<Contato> l = dao.loadAll();
		assertThat(l).size().isEqualTo(2);
		assertThat(l).containsOnly(array(c1, c2));

	}

	@Test
	public void testLoad() throws Exception {
		Contato p = dao.load(idC1);
		assertThat(p).isEqualTo(c1);

		p = dao.load(idC2);
		assertThat(p).isEqualTo(c2);

		p = dao.load(-1l);
		assertThat(p).isNull();

	}

	@Test
	public void testDelete() throws Exception {
		assertThat(dao.loadAll()).size().isEqualTo(2);
		dao.delete(idC2);
		assertThat(dao.loadAll()).size().isEqualTo(1);
		idC2 = null;
	}
	
	private void clearContatos() {
		List<Contato> l = dao.loadAll();
		for (Contato contato : l) {
			dao.delete(contato.getId());
		}
	}

}
