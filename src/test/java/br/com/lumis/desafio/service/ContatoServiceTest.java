package br.com.lumis.desafio.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.list;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.lumis.desafio.model.dao.ContatoDAO;
import br.com.lumis.desafio.model.entity.Contato;
import br.com.lumis.desafio.model.vo.ContatoVO;
import junit.framework.TestCase;

public class ContatoServiceTest extends TestCase {
	@Mock
	ContatoDAO mockedDAO;

	ContatoService service;

	Contato c1;
	Contato c2;

	@Before
	protected void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.service = new ContatoService(mockedDAO);

		c1 = new Contato();
		c1.setId(1l);
		c1.setEmail("teste1");
		c1.setNome("teste2");
		c1.setEmpresa("teste1");
		c1.setEstado("teste1");
		c1.setTelefone("teste1");
		c1.setMensagem("teste1");

		c2 = new Contato();
		c2.setId(2l);
		c2.setNome("teste2");
		c2.setEmail("teste2");
		c2.setEmpresa("teste2");
		c2.setEstado("teste2");
		c2.setTelefone("teste2");
		c2.setMensagem("teste2");

	}

	@Test
	public void testDeleteContato() {
		Long id = 21l;
		service.deleteContato(id);
		verify(mockedDAO).delete(id);
	}

	@Test
	public void testGetAll() {
		LinkedList<Contato> expected = new LinkedList<Contato>();
		expected.add(c1);
		expected.add(c2);

		when(mockedDAO.loadAll()).thenReturn(expected);
		List<ContatoVO> contatos = service.getAll();

		verify(mockedDAO, atLeastOnce()).loadAll();

		assertVoIsEqualsEntities(contatos, expected);

	}

	@Test
	public void testGetContado() {
		when(mockedDAO.load(1l)).thenReturn(c1);
		when(mockedDAO.load(2l)).thenReturn(c2);

		ContatoVO vo1 = service.getContado(1l);
		verify(mockedDAO, atLeastOnce()).load(1l);
		assertVoToEntity(vo1, c1);

		ContatoVO vo2 = service.getContado(2l);
		verify(mockedDAO, atLeastOnce()).load(2l);
		assertVoToEntity(vo2, c2);
	}

	@Test
	public void testCreateContato() {
		ContatoVO vo = new ContatoVO();
		vo.setId(12313l);
		vo.setEmail("teste");
		vo.setEmpresa("teste");
		vo.setEstado("teste");
		vo.setMensagem("teste");
		vo.setTelefone("teste");

		when(mockedDAO.create(any(Contato.class))).thenReturn(10l);
		service.createContato(vo);
		Contato entity = service.VOtoEntity(vo);
		entity.setId(null);
		verify(mockedDAO, atLeastOnce()).create(entity);
		assertThat(vo.getId()).isEqualTo(10l);

		entity.setId(10l);
		assertVoToEntity(vo, entity);

	}

	private void assertVoToEntity(ContatoVO vo, Contato expected) {
		assertThat(vo.getId()).isEqualTo(expected.getId());
		assertThat(vo.getEmail()).isEqualTo(expected.getEmail());
		assertThat(vo.getNome()).isEqualTo(expected.getNome());
		assertThat(vo.getEmpresa()).isEqualTo(expected.getEmpresa());
		assertThat(vo.getEstado()).isEqualTo(expected.getEstado());
		assertThat(vo.getMensagem()).isEqualTo(expected.getMensagem());
		assertThat(vo.getTelefone()).isEqualTo(expected.getTelefone());
	}

	private void assertVoIsEqualsEntities(List<ContatoVO> actual, LinkedList<Contato> expected) {
		for (ContatoVO vo : actual) {
			assertThat(vo.getId()).isIn(list(expected.get(0).getId(), expected.get(1).getId()));
			assertThat(vo.getNome()).isIn(list(expected.get(0).getNome(), expected.get(1).getNome()));
			assertThat(vo.getEmail()).isIn(list(expected.get(0).getEmail(), expected.get(1).getEmail()));
			assertThat(vo.getEmpresa()).isIn(list(expected.get(0).getEmpresa(), expected.get(1).getEmpresa()));
			assertThat(vo.getEstado()).isIn(list(expected.get(0).getEstado(), expected.get(1).getEstado()));
			assertThat(vo.getMensagem()).isIn(list(expected.get(0).getMensagem(), expected.get(1).getMensagem()));
			assertThat(vo.getTelefone()).isIn(list(expected.get(0).getTelefone(), expected.get(1).getTelefone()));
		}
	}

}
