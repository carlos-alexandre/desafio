package br.com.lumis.desafio.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.lumis.desafio.model.dao.UsuarioDAO;
import br.com.lumis.desafio.model.entity.Perfil;
import br.com.lumis.desafio.model.entity.Usuario;
import br.com.lumis.desafio.model.vo.PerfilVO;
import br.com.lumis.desafio.model.vo.UsuarioVO;
import junit.framework.TestCase;

public class UsuarioServiceTest extends TestCase {
	@Mock
	UsuarioDAO mockedUsuarioDAO;
	
//	@Mock
//	PerfilDAO mockedPerfilDao;

	UsuarioService service;

	Usuario u1;
	Usuario u2;

	@Before
	protected void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
//		this.service = new UsuarioService(mockedUsuarioDAO, mockedPerfilDao);
		this.service = new UsuarioService(mockedUsuarioDAO);

		u1 = new Usuario();
		u1.setNome("teste1");
		u1.setUsername("teste1");
		u1.setSenha("teste1");
		Perfil p1 = new Perfil();
		p1.setNome("teste1");
		p1.setId(1l);
		u1.setPerfil(p1);

		u2 = new Usuario();
		u2.setNome("teste2");
		u2.setUsername("teste2");
		u2.setSenha("teste2");
		Perfil p2 = new Perfil();
		p2.setNome("teste2");
		p2.setId(2l);
		u2.setPerfil(p2);

	}


	@Test
	public void testGetUsuario() {
		when(mockedUsuarioDAO.load(1l)).thenReturn(u1);
		when(mockedUsuarioDAO.load(2l)).thenReturn(u2);

		UsuarioVO vo1 = service.getUsuario(1l);
		verify(mockedUsuarioDAO, atLeastOnce()).load(1l);
		assertVoToEntity(vo1, u1);

		UsuarioVO vo2 = service.getUsuario(2l);
		verify(mockedUsuarioDAO, atLeastOnce()).load(2l);
		assertVoToEntity(vo2, u2);
	}

	@Test
	public void testGetUsuarioPorUsername() {
		when(mockedUsuarioDAO.getUsuarioPorUsername("teste1")).thenReturn(u1);
		when(mockedUsuarioDAO.getUsuarioPorUsername("teste2")).thenReturn(u2);

		UsuarioVO vo1 = service.getUsuarioPorUsername("teste1");
		verify(mockedUsuarioDAO, atLeastOnce()).getUsuarioPorUsername("teste1");
		assertVoToEntity(vo1, u1);

		UsuarioVO vo2 = service.getUsuarioPorUsername("teste2");
		verify(mockedUsuarioDAO, atLeastOnce()).getUsuarioPorUsername("teste2");
		assertVoToEntity(vo2, u2);
	}

	private void assertVoToEntity(UsuarioVO vo, Usuario expected) {
		assertThat(vo.getId()).isEqualTo(expected.getId());
		assertThat(vo.getNome()).isEqualTo(expected.getNome());
		assertThat(vo.getSenha()).isEqualTo(expected.getSenha());
		assertThat(vo.getUsername()).isEqualTo(expected.getUsername());
		assertVoToEntityPerfil(vo.getPerfil(), expected.getPerfil());
		
		
	}

	private void assertVoToEntityPerfil(PerfilVO vo, Perfil expected) {
		assertThat(vo.getId()).isEqualTo(expected.getId());
		assertThat(vo.getNome()).isEqualTo(expected.getNome());
	}



}
